[hugo]: https://github.com/HugoCarvalho99
[hugo-pic]: https://github.com/HugoCarvalho99.png?size=120
[nelson]: https://github.com/nelsonmestevao
[nelson-pic]: https://github.com/nelsonmestevao.png?size=120
[pedro]: https://github.com/pedroribeiro22
[pedro-pic]: https://github.com/pedroribeiro22.png?size=120
[celso]: https://github.com/Basto97
[celso-pic]: https://github.com/Basto97.png?size=120
[ricardo]: https://github.com/ricardoslv
[ricardo-pic]: https://github.com/ricardoslv.png?size=120

<div align="center">
    <img src="src/images/branding/logo.png" alt="delegatewise" width="350px">
</div>

## :rocket: Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes.

Start by filling out the environment variables defined in the `.env` file. Use
the `.env.sample` as a starting point.

```bash
cp .env.sample .env
```

### :inbox_tray: Prerequisites

The following software is required to be installed on your system:

- [nodejs](https://nodejs.org/en/download/)

Install all dependencies using `npm install`.

### :hammer: Development

Starting the development server.

```
npm run develop
```

Running tests.

```
npm run test
```

Lint your code.

```
npm run lint
```

Format your code.

```
npm run format
```

Thanks to [husky](https://github.com/typicode/husky), the `lint` and `format`
script will run every time you make a commit. If you receive any warnings or
your code isn't properly formatted, you should update your commit before pushing
your code.

### :package: Deployment

Build the app in netlify environment and deploy it.

```
npm run deploy
```

If you want it to be deployed in production, use `--prod`.

## :busts_in_silhouette: Team

| [![Hugo][hugo-pic]][hugo] | [![Nelson][nelson-pic]][nelson] | [![Pedro][pedro-pic]][pedro] | [![Celso][celso-pic]][celso] | [![Ricardo][ricardo-pic]][ricardo] |
| :-----------------------: | :-----------------------------: | :--------------------------: | :--------------------------: | :--------------------------------: |
|   [Hugo Carvalho][hugo]   |    [Nelson Estevão][nelson]     |    [Pedro Ribeiro][pedro]    |   [Celso Rodrigues][celso]   |      [Ricardo Silva][ricardo]      |
