import { navigate } from 'gatsby';

export const isBrowser = typeof window !== `undefined`;

export const go = (route) => {
  if (isBrowser) navigate(route);
};

export const goreload = (route) => {
  if (isBrowser) window.location = route;
};

export const getItem = (item) => {
  if (!isBrowser) return false;

  return sessionStorage.getItem(item) || localStorage.getItem(item);
};

export const isSessionPersistent = () =>
  localStorage.getItem('username') === null;

export const setItem = (key, item, persistent = true) => {
  if (!isBrowser) return false;

  if (persistent) {
    sessionStorage.setItem(key, item);
  } else {
    localStorage.setItem(key, item);
  }
  return true;
};

export const viewWidth =
  isBrowser &&
  Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);

/* eslint-disable */
// Opera 8.0+
export const isOpera =
  isBrowser &&
  ((!!window.opr && !!opr.addons) ||
    !!window.opera ||
    navigator.userAgent.indexOf(' OPR/') >= 0);

// Firefox 1.0+
export const isFirefox = typeof InstallTrigger !== 'undefined';

// Safari 3.0+ "[object HTMLElementConstructor]"
export const isSafari =
  (isBrowser && /constructor/i.test(window.HTMLElement)) ||
  (function (p) {
    return p.toString() === '[object SafariRemoteNotification]';
  })(
    isBrowser &&
      (!window.safari ||
        (typeof safari !== 'undefined' && safari.pushNotification)),
  );

// Internet Explorer 6-11
export const isIE =
  /* @cc_on!@ */ false || (isBrowser && !!document.documentMode);

// Edge 20+
export const isEdge = !isIE && isBrowser && !!window.StyleMedia;

// Chrome 1 - 79
export const isChrome =
  isBrowser &&
  !!window.chrome &&
  (!!window.chrome.webstore || !!window.chrome.runtime);

// Edge (based on chromium) detection
export const isEdgeChromium =
  isChrome && navigator.userAgent.indexOf('Edg') != -1;

// Blink engine detection
export const isBlink = (isChrome || isOpera) && !!window.CSS;
/* eslint-enable */
