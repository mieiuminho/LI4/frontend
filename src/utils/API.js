import axios from 'axios';
import { isBrowser, getItem } from './browser';

export default function API() {
  if (!isBrowser) return null;

  return axios.create({
    baseURL: process.env.GATSBY_API_ENDPOINT,
    responseType: 'json',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getItem('jwt')}`,
    },
  });
}

export const AUTH = axios.create({
  baseURL: process.env.GATSBY_API_ENDPOINT,
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
});
