import { isBrowser, getItem } from './browser';

export const isLoggedIn = () => {
  if (!isBrowser) return false;

  return getItem('isLoggedIn') === 'true';
};

export const logout = () => {
  if (!isBrowser) return;

  sessionStorage.clear();
  localStorage.clear();
};
