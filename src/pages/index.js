import React from 'react';
import { Link } from 'gatsby';
import { Button } from 'antd';
import Layout from '../layouts/BaseLayout';
import Menu from '../components/Menu';

import { isLoggedIn } from '../utils/auth';

import logo from '../images/branding/logo.png';

import routes from '../data/routes.json';
import styles from '../styles/pages/index.module.css';

const Index = () => (
  <Layout>
    {!isLoggedIn() ? (
      <div className={styles.container}>
        <img src={logo} className={styles.logo} alt="delegatewise logo" />
        <div className={styles.buttons}>
          <Link to={routes.login}>
            <Button className={styles.button} type="primary">
              Log In
            </Button>
          </Link>
          <Link to={routes.signup}>
            <Button className={styles.button} type="default">
              Sign Up
            </Button>
          </Link>
        </div>
      </div>
    ) : (
      <Menu />
    )}
  </Layout>
);

export default Index;
