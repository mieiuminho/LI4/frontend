import React from 'react';
import Layout from '../layouts/BaseLayout';
import NotFound from '../components/NotFound';

export default () => (
  <Layout>
    <NotFound />
  </Layout>
);
