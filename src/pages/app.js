import React from 'react';
import { Router } from '@reach/router';
import BaseLayout from '../layouts/BaseLayout';
import PrivateRoute from '../components/PrivateRoute';
import NotFound from '../components/NotFound';
import Profile from '../components/Profile';
import Tasks from '../components/Tasks';
import User from '../components/User';
import Workspaces from '../components/Workspaces';
import Workspace from '../components/Workspace';
import Manage from '../components/Manage';
import Reset from '../components/Reset';
import Forgot from '../components/Forgot';
import Login from '../components/Login';
import Signup from '../components/Signup';

import routes from '../data/routes.json';

const App = () => (
  <BaseLayout>
    <Router>
      <PrivateRoute
        path={`${routes.workspaces}/:workspace`}
        component={Workspace}
      />
      <PrivateRoute path={`${routes.manage}/:workspace`} component={Manage} />
      <PrivateRoute path={routes.workspaces} component={Workspaces} />
      <PrivateRoute path={routes.profile} component={Profile} />
      <PrivateRoute path={`${routes.user}/:username`} component={User} />
      <PrivateRoute path={routes.tasks} component={Tasks} />
      <Reset path={`${routes.reset}/:token`} />
      <Forgot path={`${routes.reset}`} />
      <Login path={routes.login} />
      <Signup path={routes.signup} />
      <NotFound default />
    </Router>
  </BaseLayout>
);

export default App;
