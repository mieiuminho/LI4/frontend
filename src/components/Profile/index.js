import React, { Component } from 'react';
import { message } from 'antd';
import Info from '../User/Info';

import API from '../../utils/API';
import { getItem } from '../../utils/browser';

import styles from '../../styles/components/Profile/profile.module.css';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
    };
  }

  componentDidMount() {
    API()
      .get(`${process.env.GATSBY_API_USERS}${getItem('username')}`)
      .then((response) => {
        const user = response.data;
        this.setState({ user });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  render() {
    const { user } = this.state;

    return (
      <div className={styles.container}>
        <Info user={user} />:
      </div>
    );
  }
}
