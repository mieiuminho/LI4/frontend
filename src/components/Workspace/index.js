import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Result } from 'antd';
import Schedule from '../Tasks/Schedule';
import BottomBox from './BottomBox';

import API from '../../utils/API';

import headerstyles from '../../styles/utils/headers.module.css';

export default class Workspace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      server: {
        error: false,
        status: '',
        message: '',
      },
      loading: {
        tasks: true,
      },
      workspace: { name: '' },
      tasks: {
        monday: [],
        tuesday: [],
        wednesday: [],
        thursday: [],
        friday: [],
        saturday: [],
      },
    };
  }

  componentDidMount() {
    this.getWorkspaceTasks();
    this.getWorkspaceInfo();
  }

  componentDidUpdate(prevProps) {
    const { workspace } = this.props;

    if (prevProps.workspace !== workspace) {
      this.getWorkspaceTasks();
      this.getWorkspaceInfo();
    }
  }

  getWorkspaceTasks() {
    const { workspace } = this.props;
    this.setState({ loading: { tasks: true } });
    this.setState({ server: { error: false } });

    API()
      .get(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_SCHEDULE}`,
      )
      .then((response) => {
        const tasks = response.data;
        this.setState({ tasks, loading: { tasks: false } });
      })
      .catch((error) => {
        const { status, message } = error.response.data;
        this.setState({ server: { error: true, message, status } });
      });
  }

  getWorkspaceInfo() {
    const { workspace } = this.props;

    API()
      .get(`${process.env.GATSBY_API_WORKSPACES}${workspace}/`)
      .then((response) => {
        const { name } = response.data;
        this.setState({ workspace: { name } });
      })
      .catch((error) => {
        const { status, message } = error.response.data;
        this.setState({ server: { error: true, message, status } });
      });
  }

  render() {
    const { workspace } = this.props;
    const {
      tasks,
      workspace: { name },
      loading,
      server,
    } = this.state;

    return (
      <>
        {server.error ? (
          <Result
            status={
              (server.status === 403 && '403') ||
              (server.status === 404 && '404') ||
              '500'
            }
            title={server.status}
            subTitle={server.message}
          />
        ) : (
          <>
            <h1 className={headerstyles.header}>{name}</h1>
            <Schedule schedule={tasks} workspace loading={loading.tasks} />
            <BottomBox workspace={workspace} />
          </>
        )}
      </>
    );
  }
}

Workspace.propTypes = {
  workspace: PropTypes.string.isRequired,
};
