import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import { Button, Tag, Typography, message } from 'antd';

import API from '../../utils/API';
import { getItem, goreload } from '../../utils/browser';

import routes from '../../data/routes.json';
import styles from '../../styles/components/Workspace/bottombox.module.css';

const { CheckableTag } = Tag;
const { Title } = Typography;

export default class BottomBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdmin: false,
      skills: [],
      userSkills: [],
    };
    this.leaveWorkspace = this.leaveWorkspace.bind(this);
  }

  componentDidMount() {
    this.getWorkspaceAdmin();
    this.getWorkspaceSkills();
    this.getUserWorkspaceSkills();
  }

  componentDidUpdate(prevProps) {
    const { workspace } = this.props;

    if (prevProps.workspace !== workspace) {
      this.getWorkspaceAdmin();
      this.getWorkspaceSkills();
      this.getUserWorkspaceSkills();
    }
  }

  getWorkspaceSkills() {
    const { workspace } = this.props;

    API()
      .get(`${process.env.GATSBY_API_WORKSPACES}${workspace}/skills`)
      .then((response) => {
        const skills = response.data;
        this.setState({ skills });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  getUserWorkspaceSkills() {
    const { workspace } = this.props;

    API()
      .get(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/skills/${getItem(
          'username',
        )}`,
      )
      .then((response) => {
        const userSkills = response.data;
        this.setState({ userSkills });
      })
      .catch((error) => {
        if (error?.data?.message) {
          message.error(error.response.data.message);
        }
      });
  }

  getWorkspaceAdmin() {
    const { workspace } = this.props;
    const username = getItem('username');

    API()
      .get(`${process.env.GATSBY_API_WORKSPACES}${workspace}/admin`)
      .then((response) => {
        const { admin } = response.data;
        this.setState({ isAdmin: username === admin });
      })
      .catch((error) => {
        if (error?.data?.message) {
          message.error(error.response.data.message);
        }
      });
  }

  updateUserSkills(skill, checked) {
    const { userSkills } = this.state;

    const nextSelectedSkills = checked
      ? [...userSkills, skill]
      : userSkills.filter((s) => s !== skill);

    if (checked) {
      this.postUserSkill(skill, nextSelectedSkills);
    } else {
      this.deleteUserSkill(skill, nextSelectedSkills);
    }
  }

  leaveWorkspace() {
    const { workspace } = this.props;

    API()
      .delete(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${
          process.env.GATSBY_API_USERS
        }${getItem('username')}`,
      )
      .then(() => {
        message.success(`You the left workspace!`);
        goreload(routes.workspaces);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  postUserSkill(skill, nextSelectedSkills) {
    const { workspace } = this.props;
    const username = getItem('username');
    API()
      .post(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_USERS}${username}/${process.env.GATSBY_API_SKILLS}`,
        { name: skill },
      )
      .then(() => {
        message.success(`You now have skill: ${skill}`);
        this.setState({ userSkills: nextSelectedSkills });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  deleteUserSkill(skill, nextSelectedSkills) {
    const { workspace } = this.props;
    const username = getItem('username');
    API()
      .delete(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_USERS}${username}/${process.env.GATSBY_API_SKILLS}`,
        { data: { name: skill } },
      )
      .then(() => {
        message.success(`You no longer have skill: ${skill}`);
        this.setState({ userSkills: nextSelectedSkills });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  render() {
    const { workspace } = this.props;
    const { skills, userSkills, isAdmin } = this.state;

    return (
      <div className={styles.container}>
        <Typography className={styles.skills}>
          <Title level={2}>Skills</Title>
          {skills.map((skill) => (
            <CheckableTag
              key={skill}
              checked={userSkills.includes(skill)}
              onChange={(checked) => this.updateUserSkills(skill, checked)}
            >
              {skill}
            </CheckableTag>
          ))}
        </Typography>

        <Typography className={styles.actions}>
          <Title level={2}>Actions</Title>
          {isAdmin ? (
            <Link to={`${routes.manage}/${workspace}`}>
              <Button type="primary" size="middle" className={styles.action}>
                Manage
              </Button>
            </Link>
          ) : null}
          <Button
            type="primary"
            danger
            size="middle"
            className={styles.action}
            onClick={this.leaveWorkspace}
          >
            Leave
          </Button>
        </Typography>
      </div>
    );
  }
}

BottomBox.propTypes = {
  workspace: PropTypes.string.isRequired,
};
