import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, message } from 'antd';

import API from '../../utils/API';
import { getItem, go } from '../../utils/browser';

import routes from '../../data/routes.json';
import styles from '../../styles/components/Menu/invite.module.css';

const Action = ({ type, action }) => {
  const style = {
    Accept: styles.button,
    Decline: styles.deny,
  };

  return (
    <Button className={style[type]} type="primary" onClick={action}>
      {type}
    </Button>
  );
};

export default class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      workspace: {
        id: '',
        name: '',
        image: '',
      },
    };
    this.acceptInvite = this.acceptInvite.bind(this);
    this.declineInvite = this.declineInvite.bind(this);
  }

  componentDidMount() {
    this.getWorkspaceInfo();
  }

  getWorkspaceInfo() {
    const { workspace } = this.props;

    API()
      .get(`${process.env.GATSBY_API_WORKSPACES}${workspace}`)
      .then((response) => {
        this.setState({ workspace: response.data });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  acceptInvite() {
    const { workspace } = this.props;

    API()
      .put(
        `${process.env.GATSBY_API_USERS}${getItem(
          'username',
        )}/invites/${workspace}`,
      )
      .then(() => {
        go(`${routes.workspaces}/${workspace}`);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  declineInvite() {
    const { workspace, deleteInvite } = this.props;

    API()
      .delete(
        `${process.env.GATSBY_API_USERS}${getItem(
          'username',
        )}/invites/${workspace}`,
      )
      .then(() => {
        deleteInvite(workspace);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  render() {
    const { workspace } = this.state;

    const backgroundStyle = {
      backgroundImage: `url(${workspace.image})`,
    };

    return (
      <div className={styles.invite}>
        <div>
          <div className={styles.contentWrapper}>
            <button
              type="button"
              label="workspace"
              className={styles.backgroundStyle}
              style={backgroundStyle}
            />
            <div className={styles.workspacename}>{workspace.name}</div>
          </div>
          <div className={styles.buttonWrapper}>
            <Action type="Accept" action={this.acceptInvite} />
            <Action type="Decline" action={this.declineInvite} />
          </div>
        </div>
        <div style={backgroundStyle} className={styles.background} />
      </div>
    );
  }
}

Action.propTypes = {
  type: PropTypes.oneOf(['Accept', 'Decline']).isRequired,
  action: PropTypes.func.isRequired,
};

Invite.propTypes = {
  workspace: PropTypes.string.isRequired,
  deleteInvite: PropTypes.func.isRequired,
};
