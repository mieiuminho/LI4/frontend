import React, { Component } from 'react';
import { Empty, message, Skeleton } from 'antd';
import Task from '../Tasks/Task';
import Invite from './Invite';

import API from '../../utils/API';
import { getItem } from '../../utils/browser';

import { weekdays } from '../../data/api.json';
import styles from '../../styles/components/Menu/menu.module.css';

export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: {
        invites: true,
        tasks: true,
      },
      invites: [],
      tasks: [],
    };
    this.getInvitations = this.getInvitations.bind(this);
  }

  componentDidMount() {
    this.getTasksForToday();
    this.getInvitations();
  }

  getInvitations() {
    this.setState((previousState) => {
      return {
        ...previousState,
        loading: { ...previousState.loading, invites: true },
      };
    });

    API()
      .get(`${process.env.GATSBY_API_USERS}${getItem('username')}/invites`)
      .then((response) => {
        const invites = response.data;
        this.setState((previousState) => {
          return {
            ...previousState,
            invites,
            loading: { ...previousState.loading, invites: false },
          };
        });
      })
      .catch((error) => {
        message.error(error.response.data.message);
        this.setState((previousState) => {
          return {
            ...previousState,
            loading: { ...previousState.loading, invites: false },
          };
        });
      });
  }

  getTasksForToday() {
    const today = weekdays[new Date().getDay() - 1];
    this.setState((previousState) => {
      return {
        ...previousState,
        loading: { ...previousState.loading, tasks: true },
      };
    });

    API()
      .get(
        `${process.env.GATSBY_API_SCHEDULE}${getItem(
          'username',
        )}?weekday=${today}`,
      )
      .then((response) => {
        const tasks = response.data[today];
        this.setState((previousState) => {
          return {
            ...previousState,
            tasks,
            loading: { ...previousState.loading, tasks: false },
          };
        });
      })
      .catch((error) => {
        message.error(error.response.data.message);
        this.setState((previousState) => {
          return {
            ...previousState,
            loading: { ...previousState.loading, tasks: false },
          };
        });
      });
  }

  deleteInvite = (deniedWorkspace) => {
    const { invites } = this.state;
    const newInvites = invites.filter((x) => x !== deniedWorkspace);
    this.setState({ invites: newInvites });
  };

  render() {
    const { tasks, invites, loading } = this.state;

    return (
      <div className={styles.menu}>
        <div className={styles.table} key="invites">
          <h1 className={styles.header}>Invites</h1>
          {invites.length !== 0 ? (
            <Skeleton active loading={loading.invites}>
              {invites.map((workspace) => (
                <Invite
                  key={workspace}
                  workspace={workspace}
                  deleteInvite={this.deleteInvite}
                />
              ))}
            </Skeleton>
          ) : (
            <Skeleton active loading={loading.tasks}>
              <Empty
                description="No Invitations"
                image={Empty.PRESENTED_IMAGE_SIMPLE}
              />
            </Skeleton>
          )}
        </div>

        <div className={styles.table} key="tasks">
          <h1 className={styles.header}>Tasks</h1>
          {tasks.length !== 0 ? (
            <Skeleton active loading={loading.tasks}>
              {tasks.map((task) => (
                <Task
                  key={task.id}
                  id={task.id}
                  title={task.title}
                  workspaceId={task.workspaceId}
                  workspaceName={task.workspaceName}
                  status={task.status}
                />
              ))}
            </Skeleton>
          ) : (
            <Skeleton active loading={loading.tasks}>
              <Empty
                description="No Tasks for Today"
                image={Empty.PRESENTED_IMAGE_SIMPLE}
              />
            </Skeleton>
          )}
        </div>
      </div>
    );
  }
}
