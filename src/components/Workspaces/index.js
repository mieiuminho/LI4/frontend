import React, { Component } from 'react';
import { Link } from 'gatsby';
import { message } from 'antd';

import API from '../../utils/API';

import Block from './Block';
import WorkspaceFormBlock from './WorkspaceFormBlock';

import routes from '../../data/routes.json';
import styles from '../../styles/components/Workspaces/workspaces.module.css';
import { getItem } from '../../utils/browser';

export default class Workspaces extends Component {
  constructor(props) {
    super(props);
    this.state = {
      workspaces: [],
      postWorkspace: this.postWorkspace.bind(this),
    };
  }

  componentDidMount() {
    API()
      .get(
        `${process.env.GATSBY_API_USERS}${getItem('username')}/${
          process.env.GATSBY_API_WORKSPACES
        }`,
      )
      .then((response) => {
        const workspaces = response.data;
        this.setState({ workspaces });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  postWorkspace(workspace) {
    const { workspaces } = this.state;
    const fullWs = { ...workspace, adminId: getItem('username') };
    API()
      .post(`${process.env.GATSBY_API_WORKSPACES}`, fullWs)
      .then(({ data: wsPosted }) => {
        console.log(wsPosted);
        message.success(`Workspace: ${workspace.name} was created!`);
        const newWorkspaces = workspaces.concat([wsPosted]);
        this.setState({ workspaces: newWorkspaces });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  render() {
    const { workspaces, postWorkspace } = this.state;

    return (
      <div className={styles.container}>
        {workspaces.map((workspace) => (
          <div key={workspace.id} className={styles.box}>
            <Link to={`${routes.workspaces}/${workspace.id}`}>
              <Block background={workspace.image} />
              <h1 className={styles.name}>{workspace.name}</h1>
            </Link>
          </div>
        ))}
        <div className={styles.box}>
          <WorkspaceFormBlock postMethod={postWorkspace} />
          <h1 className={styles.name}>Add new Workspace</h1>
        </div>
      </div>
    );
  }
}
