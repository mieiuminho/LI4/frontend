import React from 'react';
import { isMobile } from 'react-device-detect';
import PropTypes from 'prop-types';
import { Form, Input, Modal, Button, Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

import styles from '../../styles/components/Workspaces/block.module.css';
import stylesForms from '../../styles/components/Manage/forms.module.css';

class WorkspaceFormBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
    this.formRef = React.createRef();
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      visible: false,
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  onFinish = (values) => {
    const { postMethod } = this.props;
    postMethod({
      name: values.name,
      image: values.upload ? values.upload[0].thumbUrl.slice(22) : undefined,
    });
    this.setState({
      visible: false,
    });
    this.formRef.current.resetFields();
  };

  onReset = () => {
    this.formRef.current.resetFields();
  };

  normFile = (e) => {
    const file = Array.isArray(e) ? e?.pop() : e?.fileList?.pop();

    if (file?.name) {
      file.name = file ? file?.name?.slice(-15) : undefined;
      if (file.status === 'error') {
        file.response = 'Server Error';
      }
      return [file];
    }
    return undefined;
  };

  render() {
    const { visible } = this.state;

    return (
      <div className={styles.block}>
        <button
          type="button"
          label="addskillbutton"
          className={styles.buttonplus}
          onClick={this.showModal}
        >
          {' '}
        </button>
        <Modal
          title="Create a workspace"
          visible={visible}
          footer={[]}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form
            ref={this.formRef}
            name="control-ref"
            onFinish={this.onFinish}
            size={isMobile ? 'large' : 'middle'}
          >
            <Form.Item name="name" label="Name" rules={[{ required: true }]}>
              <Input placeholder="Insert the workspace name" />
            </Form.Item>
            <Form.Item
              name="upload"
              label="Workspace Picture"
              valuePropName="fileList"
              getValueFromEvent={this.normFile}
              rules={[{ required: true }]}
              extra="Upload your workspace picture here"
            >
              <Upload beforeUpload={() => false} name="logo" listType="picture">
                <Button>
                  <UploadOutlined /> Click to upload
                </Button>
              </Upload>
            </Form.Item>
            <Form.Item>
              <div className={stylesForms.buttonContainer}>
                <Button
                  type="primary"
                  className={stylesForms.formButton}
                  htmlType="submit"
                >
                  Create Workspace
                </Button>
                <Button
                  htmlType="button"
                  className={stylesForms.formButton}
                  onClick={this.onReset}
                >
                  Reset
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default WorkspaceFormBlock;

WorkspaceFormBlock.propTypes = {
  postMethod: PropTypes.func.isRequired,
};
