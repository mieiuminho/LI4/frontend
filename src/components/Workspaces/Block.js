import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../styles/components/Workspaces/block.module.css';

function Action({ background }) {
  const backgroundStyle = {
    backgroundImage: `url(${background})`,
  };

  return (
    <button
      type="button"
      label="wsBlock"
      style={backgroundStyle}
      className={styles.button}
    />
  );
}

const Block = ({ background }) => (
  <div className={styles.block}>
    <Action background={background} />
  </div>
);

export default Block;

Action.propTypes = {
  background: PropTypes.string.isRequired,
};

Block.propTypes = {
  background: PropTypes.string.isRequired,
};
