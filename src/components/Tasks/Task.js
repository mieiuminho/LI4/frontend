import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import { Button, Card, Row, Avatar, message, Tooltip } from 'antd';
import {
  CheckOutlined,
  CheckCircleTwoTone,
  ExclamationCircleTwoTone,
  CloseOutlined,
  CloseCircleTwoTone,
  ClockCircleOutlined,
} from '@ant-design/icons';

import API from '../../utils/API';
import { tasks } from '../../data/api.json';

import routes from '../../data/routes.json';
import styles from '../../styles/components/Tasks/task.module.css';

const { Meta } = Card;

const Status = ({ status }) => {
  const colors = {
    COMPLETED: '#52c41a',
    PENDING: '#575757',
    LATE: '#fa8c16',
    FAILED: '#f5222d',
  };

  const icons = {
    COMPLETED: <CheckCircleTwoTone twoToneColor={colors[status]} />,
    PENDING: <ClockCircleOutlined />,
    LATE: <ExclamationCircleTwoTone twoToneColor={colors[status]} />,
    FAILED: <CloseCircleTwoTone twoToneColor={colors[status]} />,
  };

  const style = {
    color: `${colors[status]}`,
  };

  return (
    <div className={styles.status} style={style}>
      {status} {icons[status]}
    </div>
  );
};

const Check = ({ status, action }) => {
  const option = {
    COMPLETED: (
      <Button
        danger
        onClick={() => action() && console.log('marked as uncompleted')}
      >
        MARK AS UNCOMPLETE <CloseOutlined />
      </Button>
    ),
    PENDING: (
      <Button onClick={() => action() && console.log('marked as done')}>
        MARK AS DONE <CheckOutlined />
      </Button>
    ),
    LATE: (
      <Button onClick={() => action() && console.log('marked as done')}>
        MARK AS DONE <CheckOutlined />
      </Button>
    ),
    FAILED: null,
  };

  return option[status];
};

export default class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatar: '',
      status: props.status,
    };
    this.updateStatus = this.updateStatus.bind(this);
  }

  componentDidMount() {
    const { assignee } = this.props;

    if (assignee) {
      this.getUserPhoto();
    }
  }

  getUserPhoto() {
    const { assignee } = this.props;

    API()
      .get(`${process.env.GATSBY_API_USERS}${assignee}`)
      .then((response) => {
        const { avatar } = response.data;
        this.setState({ avatar });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  updateStatus() {
    const { id, workspaceId } = this.props;

    API()
      .put(
        `${process.env.GATSBY_API_WORKSPACES}${workspaceId}/${process.env.GATSBY_API_TASKS}${id}/status`,
      )
      .then((response) => {
        const { status } = response.data;
        this.setState({ status });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  render() {
    const { title, description, workspaceName, assignee } = this.props;
    const { avatar, status } = this.state;

    return (
      <Card
        title={workspaceName}
        size="small"
        className={styles.task}
        extra={<Status status={status} />}
      >
        <div className={styles.metadata}>
          <Meta
            avatar={
              assignee ? (
                <Link to={`${routes.user}/${assignee}`}>
                  <Tooltip title={`@${assignee}`}>
                    <Avatar src={avatar}>
                      {assignee.charAt(0).toUpperCase()}
                    </Avatar>
                  </Tooltip>
                </Link>
              ) : null
            }
            title={<Tooltip title={title}>{title}</Tooltip>}
            description={description}
          />
        </div>

        {status ? (
          <Row justify="center" align="middle">
            <Check status={status} action={this.updateStatus} />
          </Row>
        ) : null}
      </Card>
    );
  }
}

Status.propTypes = {
  status: PropTypes.oneOf(tasks.status).isRequired,
};

Task.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  workspaceId: PropTypes.string.isRequired,
  workspaceName: PropTypes.string,
  status: PropTypes.oneOf(tasks.status).isRequired,
  assignee: PropTypes.string,
};
