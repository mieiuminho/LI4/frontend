import React from 'react';
import PropTypes from 'prop-types';
import { Empty, Skeleton } from 'antd';
import Task from './Task';

import { weekdays, tasks } from '../../data/api.json';
import styles from '../../styles/components/Tasks/schedule.module.css';

const Schedule = ({ schedule, workspace = false, loading = false }) => (
  <div className={styles.schedule}>
    {weekdays.map((day) => (
      <div className={styles.day} key={day}>
        {schedule?.[day] ? (
          <>
            <h1 className={styles.header}>{day}</h1>
            {schedule[day].length !== 0 ? (
              <Skeleton active loading={loading}>
                {schedule[day].map((task) => (
                  <Task
                    key={task.id}
                    id={task.id}
                    title={task.title}
                    description={task.description}
                    workspaceId={task.workspaceId}
                    workspaceName={workspace ? null : task.workspaceName}
                    status={task.status}
                    assignee={workspace ? task.assignee : null}
                  />
                ))}
              </Skeleton>
            ) : (
              <Skeleton active loading={loading}>
                <Empty
                  description="No Tasks"
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                />
              </Skeleton>
            )}
          </>
        ) : null}
      </div>
    ))}
  </div>
);

export default Schedule;

const tasksType = PropTypes.arrayOf(
  PropTypes.exact({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    workspaceId: PropTypes.string.isRequired,
    workspaceName: PropTypes.string,
    status: PropTypes.oneOf(tasks.status).isRequired,
    assignee: PropTypes.string,
  }),
).isRequired;

Schedule.propTypes = {
  schedule: PropTypes.exact({
    monday: tasksType,
    tuesday: tasksType,
    wednesday: tasksType,
    thursday: tasksType,
    friday: tasksType,
    saturday: tasksType,
  }).isRequired,
  workspace: PropTypes.bool,
  loading: PropTypes.bool.isRequired,
};
