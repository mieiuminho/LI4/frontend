import React, { Component } from 'react';
import { message } from 'antd';
import Schedule from './Schedule';

import API from '../../utils/API';
import { getItem } from '../../utils/browser';

export default class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      tasks: {
        monday: [],
        tuesday: [],
        wednesday: [],
        thursday: [],
        friday: [],
        saturday: [],
      },
    };
  }

  componentDidMount() {
    API()
      .get(`${process.env.GATSBY_API_SCHEDULE}${getItem('username')}`)
      .then((response) => {
        const tasks = response.data;
        this.setState({ tasks, loading: false });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  render() {
    const { loading, tasks } = this.state;

    return <Schedule schedule={tasks} loading={loading} />;
  }
}
