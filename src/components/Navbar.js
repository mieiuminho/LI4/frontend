import React, { Component } from 'react';
import { Link } from 'gatsby';
import { Menu, Layout, Avatar, message } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import ProfileDrawer from './ProfileDrawer';

import API from '../utils/API';
import { isLoggedIn, logout } from '../utils/auth';
import { go, getItem, setItem, isSessionPersistent } from '../utils/browser';

import icon from '../images/branding/icon.png';
import delegate from '../images/branding/delegate.png';

import routes from '../data/routes.json';
import styles from '../styles/components/navbar.module.css';

const { Header } = Layout;
const { SubMenu } = Menu;

class Items extends Component {
  constructor(props) {
    super(props);
    this.state = {
      workspaces: [],
      visibleDrawer: false,
      updates: 0,
      user: {},
    };
  }

  componentDidMount() {
    API()
      .get(`${process.env.GATSBY_API_USERS}${getItem('username')}`)
      .then((response) => {
        const user = response.data;
        this.setState({ user });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });

    API()
      .get(
        `${process.env.GATSBY_API_USERS}${getItem('username')}/${
          process.env.GATSBY_API_WORKSPACES
        }`,
      )
      .then((response) => {
        const workspaces = response.data;
        this.setState({ workspaces });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  showDrawer = () => this.setState({ visibleDrawer: true });

  hideDrawer = () => this.setState({ visibleDrawer: false });

  updateUser = ({
    username,
    name,
    email,
    upload,
    password,
    currentPassword,
  }) => {
    const { updates } = this.state;
    const user = {
      username,
      name,
      email,
      password: password || null,
      avatar: (upload && upload[0]?.thumbUrl?.slice(22)) || null,
      currentPassword,
    };
    API()
      .put(`${process.env.GATSBY_API_USERS}${getItem('username')}`, user)
      .then((response) => {
        const {
          email: newemail,
          name: newname,
          username: newusername,
          avatar: newavatar,
          token: newtoken,
        } = response.data;

        this.setState({
          updates: updates + 1,
          user: {
            email: newemail,
            name: newname,
            username: newusername,
            avatar: newavatar,
          },
        });
        const persistence = isSessionPersistent();
        setItem('username', newusername, persistence);
        setItem('jwt', newtoken, persistence);
        message.success('Your profile has been updated!');
        this.hideDrawer();
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  };

  deleteUser = () => {
    API()
      .delete(`${process.env.GATSBY_API_USERS}${getItem('username')}`)
      .then(() => {
        localStorage.clear();
        sessionStorage.clear();
        message.success('Your account has been deleted');
        go(routes.login);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
    console.log(`${getItem('username')} got fucked`);
  };

  getCurrentPage = () => {
    let selected = null;

    Object.keys(routes).forEach((key) => {
      if (routes[key] === window.location.pathname) selected = key;
    });

    return selected;
  };

  leave = () => {
    logout();
    go(routes.home);
  };

  render() {
    const { workspaces, updates, visibleDrawer, user } = this.state;

    return (
      <>
        <Menu
          selectedKeys={this.getCurrentPage()}
          className={styles.menu}
          mode="horizontal"
        >
          <Menu.Item key="tasks">
            <Link to={routes.tasks}>Tasks</Link>
          </Menu.Item>

          <SubMenu
            key="workspaces"
            title={
              <Link className={styles.workspaces} to={routes.workspaces}>
                Workspaces
              </Link>
            }
          >
            {workspaces.map((workspace) => (
              <Menu.Item key={workspace.name}>
                <Link to={`${routes.workspaces}/${workspace.id}`}>
                  {workspace.name}
                </Link>
              </Menu.Item>
            ))}
          </SubMenu>

          <SubMenu
            title={
              <Link to={`${routes.profile}`}>
                <Avatar src={user ? user.avatar : null} icon={<UserOutlined />}>
                  {user ? user.name : null}
                </Avatar>
              </Link>
            }
          >
            <Menu.Item key="profiledrawer" onClick={this.showDrawer}>
              Update Profile
            </Menu.Item>
            <Menu.Item
              key="profile"
              className={styles.logout}
              onClick={() => this.leave()}
            >
              Log out
            </Menu.Item>
          </SubMenu>
        </Menu>
        <ProfileDrawer
          visible={visibleDrawer}
          user={user}
          updates={updates}
          hideDrawer={this.hideDrawer}
          updateUser={this.updateUser}
          deleteUser={this.deleteUser}
        />
      </>
    );
  }
}

const Navbar = () => (
  <Header className={styles.intro}>
    <Link to={routes.home}>
      <img src={icon} className={styles.icon} alt="delegatewise's logo" />
      <img src={delegate} className={styles.delegate} alt="delegatewise" />
    </Link>
    {isLoggedIn() ? (
      <Items />
    ) : (
      <Menu className={styles.menu} mode="horizontal" />
    )}
  </Header>
);

export default Navbar;
