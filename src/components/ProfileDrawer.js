import React from 'react';
import PropTypes from 'prop-types';
import { isMobile } from 'react-device-detect';
import { Drawer, Form, Input, Button, Upload, Popconfirm } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

import { viewWidth } from '../utils/browser';

const normFile = (e) => {
  const file = Array.isArray(e) ? e?.pop() : e?.fileList?.pop();

  if (file?.name) {
    file.name = file ? file?.name?.slice(-15) : undefined;
    if (file.status === 'error') {
      file.response = 'Server Error';
    }
    return [file];
  }
  return undefined;
};

const DeleteButton = ({ deleteUser }) => {
  return (
    <Popconfirm
      title="Are you sure you want to delete you account?"
      onConfirm={deleteUser}
      okText="Yes"
      cancelText="No"
    >
      <Button danger type="ghost" style={{ marginLeft: '50px' }}>
        Delete Account
      </Button>
    </Popconfirm>
  );
};

const ProfileDrawer = ({
  visible,
  user,
  updates,
  hideDrawer,
  updateUser,
  deleteUser,
}) => {
  return (
    <Drawer
      title="Update your Account"
      width={isMobile ? viewWidth : 520}
      onClose={hideDrawer}
      visible={visible}
      bodyStyle={{ paddingBottom: 80 }}
    >
      <Form
        name="register"
        size={isMobile ? 'large' : 'middle'}
        onFinish={updateUser}
        key={updates}
      >
        <Form.Item
          name="email"
          label="E-mail"
          initialValue={user?.email}
          rules={[
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            {
              message: 'Please input your E-mail!',
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="password"
          label="New Password"
          rules={[
            {
              message: 'Please input your password!',
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="nconfirm"
          label="Confirm new Password"
          dependencies={['npassword']}
          hasFeedback
          rules={[
            {
              message: 'Please confirm your password!',
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error('The two passwords that you entered do not match!'),
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="name"
          label="Display Name"
          initialValue={user?.name}
          rules={[
            {
              message: 'Please input your real name!',
              whitespace: true,
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="username"
          label="Username"
          initialValue={user?.username}
          rules={[{ required: true }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="upload"
          label="Profile Picture"
          valuePropName="fileList"
          getValueFromEvent={normFile}
          extra="Upload your profile picture here"
        >
          <Upload beforeUpload={() => false} name="logo" listType="picture">
            <Button>
              <UploadOutlined /> Click to upload
            </Button>
          </Upload>
        </Form.Item>
        <Form.Item
          name="currentPassword"
          label="Password"
          rules={[
            {
              message: 'Please input your password!',
              required: true,
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            style={{ marginLeft: '20px' }}
            htmlType="submit"
          >
            Update profile
          </Button>
          <DeleteButton deleteUser={deleteUser} />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

ProfileDrawer.propTypes = {
  visible: PropTypes.bool,
  user: PropTypes.object,
  updates: PropTypes.number.isRequired,
  hideDrawer: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
};

DeleteButton.propTypes = {
  deleteUser: PropTypes.func.isRequired,
};

export default ProfileDrawer;
