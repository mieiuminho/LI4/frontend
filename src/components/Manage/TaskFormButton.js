import React from 'react';
import { isMobile } from 'react-device-detect';
import PropTypes from 'prop-types';
import { Form, Input, Select, Modal, Button, Slider, Switch } from 'antd';

import stylesButtons from '../../styles/components/Manage/buttons-5margin.module.css';
import stylesManage from '../../styles/components/Manage/manage.module.css';
import stylesForms from '../../styles/components/Manage/forms.module.css';

import { taskfrequency } from '../../data/api.json';

class TaskFormButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
    this.formRef = React.createRef();
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  sliderChange = (value) => {
    this.formRef.current.setFieldsValue({
      workload: value,
    });
  };

  skillChange = (value) => {
    this.formRef.current.setFieldsValue({
      skills: value,
    });
  };

  freqChange = (value) => {
    this.formRef.current.setFieldsValue({
      freq: value,
    });
  };

  handleOk = () => {
    this.setState({
      visible: false,
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  onFinish = (formTask) => {
    const { task, updateMethod, postMethod } = this.props;

    if (task != null) {
      const newTask = {
        id: task.id,
        ...formTask,
      };
      updateMethod(newTask);
    } else {
      const newTask = {
        id: null,
        ...formTask,
      };
      postMethod(newTask);
    }

    this.formRef.current.resetFields();
    this.setState({
      visible: false,
    });
  };

  onReset = () => {
    this.formRef.current.resetFields();
  };

  clickable() {
    let button = null;
    const { type } = this.props;
    switch (type) {
      case 'Edit':
        button = (
          <Button
            type="primary"
            className={stylesButtons.yellowbtnSmall}
            onClick={this.showModal}
          >
            Edit
          </Button>
        );
        break;
      case 'Add':
        button = (
          <button
            type="button"
            label="addtaskbutton"
            className={stylesManage.bigplusButton}
            onClick={this.showModal}
          >
            {' '}
          </button>
        );
        break;
      default:
        button = null;
    }
    return button;
  }

  render() {
    const { Option } = Select;
    const { visible } = this.state;
    const { task } = this.props;

    const { workspaceskills } = this.props;
    const sendFormButtonText = task ? 'Edit task' : 'Add task';

    return (
      <div>
        {this.clickable()}
        <Modal
          title={task ? 'Add a task' : 'Create a task'}
          visible={visible}
          footer={[]}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form
            ref={this.formRef}
            name="control-ref"
            size={isMobile ? 'large' : 'middle'}
            onFinish={this.onFinish}
          >
            <Form.Item
              name="title"
              key="title"
              label="Title"
              initialValue={task ? task.title : undefined}
              rules={[{ required: true }]}
            >
              <Input placeholder="Insert the task title" />
            </Form.Item>
            <Form.Item
              name="description"
              key="description"
              label="Description"
              initialValue={task ? task.description : ''}
            >
              <Input placeholder="Write a quick description about the task" />
            </Form.Item>
            <Form.Item
              name="freq"
              key="freq"
              label="Frequency"
              initialValue={task ? task.freq : undefined}
              rules={[{ required: true }]}
            >
              <Select placeholder="Choose the frequency" allowClear>
                {Object.keys(taskfrequency).map((key) => (
                  <Option value={key} key={key}>
                    {taskfrequency[key]}
                  </Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item
              name="skillset"
              key="skillset"
              label="Skill"
              initialValue={task ? task.skillset : ''}
            >
              <Select
                style={{ width: '100%' }}
                allowClear
                placeholder="Choose the necessary skills"
                optionLabelProp="label"
              >
                {workspaceskills.map((skill) => (
                  <Option value={skill} key={skill}>
                    {skill}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="flexible"
              key="flexible"
              label="Flexible"
              valuePropName="checked"
              initialValue={task ? task.flexible : false}
            >
              <Switch />
            </Form.Item>
            <Form.Item
              name="workload"
              key="workload"
              label="Pick workload"
              initialValue={task ? task.workload : undefined}
              rules={[{ required: true }]}
            >
              <Slider min={0} max={5} step={0.1} />
            </Form.Item>
            <Form.Item key="actions">
              <div className={stylesForms.buttonContainer}>
                <Button
                  type="primary"
                  className={stylesForms.formButton}
                  htmlType="submit"
                >
                  {sendFormButtonText}
                </Button>
                <Button
                  htmlType="button"
                  className={stylesForms.formButton}
                  onClick={this.onReset}
                >
                  Reset
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default TaskFormButton;

TaskFormButton.propTypes = {
  task: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    freq: PropTypes.oneOf(Object.keys(taskfrequency)).isRequired,
    flexible: PropTypes.bool.isRequired,
    skillset: PropTypes.string.isRequired,
    workload: PropTypes.number.isRequired,
    workspace: PropTypes.string.isRequired,
  }),
  workspaceskills: PropTypes.arrayOf(PropTypes.string).isRequired,
  type: PropTypes.string.isRequired,
  updateMethod: PropTypes.func,
  postMethod: PropTypes.func,
};
