import React from 'react';
import { isMobile } from 'react-device-detect';
import PropTypes from 'prop-types';
import { Form, Input, Modal, Button } from 'antd';

import stylesManage from '../../styles/components/Manage/manage.module.css';
import stylesForms from '../../styles/components/Manage/forms.module.css';

class SkillFormButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
    this.formRef = React.createRef();
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  onFinish = (skill) => {
    const { postMethod } = this.props;
    postMethod(skill.name);
    this.setState({
      visible: false,
    });
    this.formRef.current.resetFields();
  };

  onReset = () => {
    this.formRef.current.resetFields();
  };

  render() {
    const { visible } = this.state;

    return (
      <div>
        <button
          type="button"
          label="addskillbutton"
          className={stylesManage.bigplusButton}
          onClick={this.showModal}
        >
          {' '}
        </button>
        <Modal
          title="Add a skill"
          visible={visible}
          footer={[]}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form
            ref={this.formRef}
            name="control-ref"
            size={isMobile ? 'large' : 'middle'}
            onFinish={this.onFinish}
          >
            <Form.Item name="name" label="Title" rules={[{ required: true }]}>
              <Input placeholder="Insert the skill title" />
            </Form.Item>
            <Form.Item>
              <div className={stylesForms.buttonContainer}>
                <Button
                  type="primary"
                  className={stylesForms.formButton}
                  htmlType="submit"
                >
                  Add skill
                </Button>
                <Button
                  htmlType="button"
                  className={stylesForms.formButton}
                  onClick={this.onReset}
                >
                  Reset
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default SkillFormButton;

SkillFormButton.propTypes = {
  postMethod: PropTypes.func.isRequired,
};
