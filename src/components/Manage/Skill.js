import React from 'react';
import PropTypes from 'prop-types';
import { Button, Skeleton, Card } from 'antd';

import styles from '../../styles/components/Manage/buttons-5margin.module.css';
import cardstyle from '../../styles/utils/cards.module.css';

const Skill = ({ skill, deleteMethod }) => {
  const { Meta } = Card;

  return (
    <Card
      id={skill}
      size="small"
      className={cardstyle.card}
      bordered
      actions={[
        <Button
          className={styles.redbtn}
          type="primary"
          onClick={() => {
            deleteMethod(skill);
          }}
        >
          Remove
        </Button>,
      ]}
    >
      <Skeleton loading={false} avatar active>
        <Meta title={<span className={cardstyle.title}>{skill}</span>} />
      </Skeleton>
    </Card>
  );
};

export default Skill;

Skill.propTypes = {
  skill: PropTypes.string.isRequired,
  deleteMethod: PropTypes.func.isRequired,
};
