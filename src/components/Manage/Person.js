import React from 'react';
import PropTypes from 'prop-types';
import { Button, Skeleton, Card, Avatar } from 'antd';

import styles from '../../styles/components/Manage/buttons-5margin.module.css';
import cardstyle from '../../styles/utils/cards.module.css';

const Person = ({ user, deleteMethod }) => {
  const { Meta } = Card;

  return (
    <Card
      id={user.username}
      size="small"
      className={cardstyle.card}
      bordered
      actions={[
        <Button
          className={styles.redbtn}
          type="primary"
          onClick={() => deleteMethod(user.username)}
        >
          Remove
        </Button>,
      ]}
    >
      <Skeleton loading={false} avatar active>
        <Meta
          avatar={<Avatar src={user.avatar} />}
          title={<span className={cardstyle.title}>{user.name}</span>}
        />
      </Skeleton>
    </Card>
  );
};

export default Person;

Person.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string,
  }).isRequired,
  deleteMethod: PropTypes.func.isRequired,
};
