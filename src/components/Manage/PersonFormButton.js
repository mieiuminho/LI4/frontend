import React, { Component } from 'react';
import { isMobile } from 'react-device-detect';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  AutoComplete,
  Button,
  Input,
  Form,
  Modal,
  message,
  Avatar,
} from 'antd';
import { UserOutlined } from '@ant-design/icons';

import API from '../../utils/API';
import { getItem } from '../../utils/browser';

import stylesManage from '../../styles/components/Manage/manage.module.css';
import stylesForms from '../../styles/components/Manage/forms.module.css';

const Username = styled.span`
  color: gray;
`;

const renderUser = (user) => ({
  value: user.username,
  name: user.name,
  label: (
    <>
      <Avatar src={user.avatar} icon={<UserOutlined />} /> {user.name}{' '}
      <Username>@{user.username}</Username>
    </>
  ),
});

class SkillFormButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      users: [],
    };
    this.formRef = React.createRef();
  }

  componentDidMount() {
    API()
      .get(`${process.env.GATSBY_API_USERS}`)
      .then((response) => {
        const users = response.data.filter(
          (user) => user.username !== getItem('username'),
        );
        this.setState({ users });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  onFinish = (user) => {
    const { postMethod } = this.props;
    postMethod(user.username);
    this.setState({
      visible: false,
    });
    this.formRef.current.resetFields();
  };

  onReset = () => {
    this.formRef.current.resetFields();
  };

  render() {
    const { visible, users } = this.state;

    return (
      <div>
        <button
          type="button"
          label="inviteuserbutton"
          className={stylesManage.bigplusButton}
          onClick={this.showModal}
        >
          {' '}
        </button>
        <Modal
          title="Invite someone to your workspace"
          visible={visible}
          footer={[]}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form
            ref={this.formRef}
            name="control-ref"
            size={isMobile ? 'large' : 'middle'}
            onFinish={this.onFinish}
          >
            <Form.Item name="username" rules={[{ required: true }]}>
              <AutoComplete
                options={users.map((user) => renderUser(user))}
                filterOption={(inputValue, option) => {
                  return (
                    option.name
                      .toUpperCase()
                      .indexOf(inputValue.toUpperCase()) !== -1 ||
                    option.value
                      .toUpperCase()
                      .indexOf(inputValue.toUpperCase()) !== -1
                  );
                }}
              >
                <Input
                  placeholder="Enter the user's username"
                  prefix={<UserOutlined />}
                />
              </AutoComplete>
            </Form.Item>
            <Form.Item>
              <div className={stylesForms.buttonContainer}>
                <Button
                  type="primary"
                  className={stylesForms.formButton}
                  htmlType="submit"
                >
                  Invite
                </Button>
                <Button
                  htmlType="button"
                  className={stylesForms.formButton}
                  onClick={this.onReset}
                >
                  Reset
                </Button>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default SkillFormButton;

SkillFormButton.propTypes = {
  postMethod: PropTypes.func.isRequired,
};
