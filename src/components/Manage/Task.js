import React from 'react';
import PropTypes from 'prop-types';
import { Button, Skeleton, Card } from 'antd';
import TaskFormButton from './TaskFormButton';

import styles from '../../styles/components/Manage/buttons-5margin.module.css';
import cardstyle from '../../styles/utils/cards.module.css';

import { taskfrequency } from '../../data/api.json';

const Task = ({ task, workspaceskills, deleteMethod, updateMethod }) => {
  const { Meta } = Card;

  return (
    <Card
      id={task.id}
      size="small"
      className={cardstyle.card}
      bordered
      actions={[
        <TaskFormButton
          task={task}
          workspaceskills={workspaceskills}
          updateMethod={updateMethod}
          type="Edit"
        />,
        <Button
          className={styles.redbtnSmall}
          type="primary"
          onClick={() => {
            deleteMethod(task);
          }}
        >
          Remove
        </Button>,
      ]}
    >
      <Skeleton loading={false} avatar active>
        <div className={cardstyle.skeletonflex}>
          <Meta
            title={<span className={cardstyle.title}>{task.title}</span>}
            description={task.description}
          />
          <div className={cardstyle.fakegridrow}>
            {task.skillset ? `Skill: ${task.skillset}` : 'No skill required'}
          </div>
          <div className={cardstyle.fakegridrow}>
            Freq: {taskfrequency[task.freq]}
          </div>
          <div className={cardstyle.fakegridrow}>Workload: {task.workload}</div>
        </div>
      </Skeleton>
    </Card>
  );
};

export default Task;

Task.propTypes = {
  task: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    freq: PropTypes.oneOf(Object.keys(taskfrequency)).isRequired,
    skillset: PropTypes.string.isRequired,
    workload: PropTypes.number.isRequired,
  }).isRequired,
  deleteMethod: PropTypes.func.isRequired,
  updateMethod: PropTypes.func.isRequired,
  workspaceskills: PropTypes.arrayOf(PropTypes.string).isRequired,
};
