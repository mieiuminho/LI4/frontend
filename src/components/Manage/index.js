import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { message, Skeleton } from 'antd';
import Task from './Task';
import Person from './Person';
import Skill from './Skill';
import TaskFormButton from './TaskFormButton';
import SkillFormButton from './SkillFormButton';
import PersonFormButton from './PersonFormButton';

import API from '../../utils/API';
import { getItem } from '../../utils/browser';

import styles from '../../styles/components/Manage/manage.module.css';
import headerstyles from '../../styles/utils/headers.module.css';

export default class Manage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: {
        users: true,
        skills: true,
        tasks: true,
      },
      workspace: { name: '' },
      users: [],
      skills: [],
      tasks: [],
    };
    this.deleteSkill = this.deleteSkill.bind(this);
    this.postSkill = this.postSkill.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.postUser = this.postUser.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
    this.updateTask = this.updateTask.bind(this);
    this.postTask = this.postTask.bind(this);
  }

  componentDidMount() {
    const { workspace } = this.props;
    this.setState((previousState) => {
      return {
        ...previousState,
        loading: {
          ...previousState.loading,
          invites: true,
          skills: true,
          tasks: true,
        },
      };
    });

    API()
      .get(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_SKILLS}`,
      )
      .then((response) => {
        const skills = response.data;
        this.setState((previousState) => {
          return {
            ...previousState,
            skills,
            loading: { ...previousState.loading, skills: false },
          };
        });
      })
      .catch((error) => {
        message.error(error.response.data.message);
        this.setState((previousState) => {
          return {
            ...previousState,
            loading: { ...previousState.loading, skills: false },
          };
        });
      });

    API()
      .get(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_TASKS}`,
      )
      .then((response) => {
        const tasks = response.data;
        this.setState((previousState) => {
          return {
            ...previousState,
            tasks,
            loading: { ...previousState.loading, tasks: false },
          };
        });
      })
      .catch((error) => {
        message.error(error.response.data.message);
        this.setState((previousState) => {
          return {
            ...previousState,
            loading: { ...previousState.loading, tasks: false },
          };
        });
      });

    API()
      .get(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_USERS}`,
      )
      .then((response) => {
        const users = response.data.filter(
          (user) => user.username !== getItem('username'),
        );
        this.setState((previousState) => {
          return {
            ...previousState,
            users,
            loading: { ...previousState.loading, users: false },
          };
        });
      })
      .catch((error) => {
        message.error(error.response.data.message);
        this.setState((previousState) => {
          return {
            ...previousState,
            loading: { ...previousState.loading, users: false },
          };
        });
      });

    API()
      .get(`${process.env.GATSBY_API_WORKSPACES}${workspace}/`)
      .then((response) => {
        const { name } = response.data;
        this.setState({ workspace: { name } });
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  deleteSkill(skill) {
    const { workspace } = this.props;
    const { skills } = this.state;
    API()
      .delete(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_SKILLS}`,
        { data: { name: skill } },
      )
      .then(() => {
        const newSkills = skills.filter((x) => x !== skill);
        this.setState({ skills: newSkills });
        message.success(`Skill: ${skill} was removed from the workspace`);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  postSkill(skill) {
    const { workspace } = this.props;
    const { skills } = this.state;
    API()
      .post(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_SKILLS}`,
        { name: skill },
      )
      .then(() => {
        const newSkills = skills.concat([skill]);
        this.setState({ skills: newSkills });
        message.success(`Skill: ${skill} was added to the workspace`);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  deleteUser(username) {
    const { workspace } = this.props;
    const { users } = this.state;
    API()
      .delete(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_USERS}${username}`,
      )
      .then(() => {
        const newUsers = users.filter((x) => x.username !== username);
        this.setState({ users: newUsers });
        message.success(`User: ${username} was removed from the workspace`);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  postUser(username) {
    const { workspace } = this.props;
    API()
      .post(`${process.env.GATSBY_API_USERS}${username}/invites/${workspace}`)
      .then(() => {
        message.success(`User: ${username} was invited to the workspace`);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  deleteTask(task) {
    const { workspace } = this.props;
    const { tasks } = this.state;
    API()
      .delete(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_TASKS}${task.id}`,
      )
      .then(() => {
        const newTasks = tasks.filter((x) => x.id !== task.id);
        this.setState({ tasks: newTasks });
        message.success(`Task: ${task.title} was removed from the workspace`);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  updateTask(newtask) {
    const { workspace } = this.props;
    const { tasks } = this.state;
    const task = { workspace, ...newtask };
    API()
      .put(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_TASKS}${task.id}`,
        task,
      )
      .then(() => {
        const newTasks = tasks.filter((x) => x.id !== task.id).concat([task]);
        this.setState({ tasks: newTasks });
        message.success(`Task: ${task.title} was updated `);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  postTask(newtask) {
    const { workspace } = this.props;
    const { tasks } = this.state;
    API()
      .post(
        `${process.env.GATSBY_API_WORKSPACES}${workspace}/${process.env.GATSBY_API_TASKS}`,
        { workspace, ...newtask },
      )
      .then((response) => {
        // TODO: Verificar de funciona depois do update
        const task = response.data;
        const newTasks = tasks.concat([task]);
        this.setState({ tasks: newTasks });
        message.success(`Task: ${task.title} was added to the workspace `);
      })
      .catch((error) => {
        message.error(error.response.data.message);
      });
  }

  render() {
    const { tasks, skills, users, workspace, loading } = this.state;

    return (
      <div className={styles.menu} key="managewrapper">
        <h1 className={headerstyles.header}>{workspace.name}</h1>
        <div className={styles.table} key="invites">
          <h1 className={styles.header}>People</h1>
          <Skeleton active loading={loading.users}>
            {users.map((user) => (
              <Person
                key={user.username}
                id={user.username}
                user={user}
                deleteMethod={this.deleteUser}
              />
            ))}
          </Skeleton>
          <PersonFormButton postMethod={this.postUser} />
        </div>
        <div className={styles.table} key="skills">
          <h1 className={styles.header}>Skills</h1>
          <Skeleton active loading={loading.skills}>
            {skills.map((skill) => (
              <Skill
                key={skill}
                id={skill}
                skill={skill}
                deleteMethod={this.deleteSkill}
              />
            ))}
          </Skeleton>
          <SkillFormButton postMethod={this.postSkill} />
        </div>
        <div className={styles.table} key="tasks">
          <h1 className={styles.header}>Tasks</h1>
          <Skeleton active loading={loading.tasks}>
            {tasks.map((task) => (
              <Task
                key={task.id}
                task={task}
                workspaceskills={skills}
                deleteMethod={this.deleteTask}
                updateMethod={this.updateTask}
              />
            ))}
          </Skeleton>
          <TaskFormButton
            task={null}
            workspaceskills={skills}
            postMethod={this.postTask}
            type="Add"
          />
        </div>
      </div>
    );
  }
}

Manage.propTypes = {
  workspace: PropTypes.string.isRequired,
};
