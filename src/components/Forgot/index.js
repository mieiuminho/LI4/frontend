import React, { useState } from 'react';
import { isMobile } from 'react-device-detect';
import { Link } from 'gatsby';
import { Form, Input, Button, Result, Spin } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import Title from '../Title';

import { AUTH } from '../../utils/API';

import routes from '../../data/routes.json';
import styles from '../../styles/components/Login/login.module.css';

export default () => {
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState('');
  const [fail, setFail] = useState('');

  const onFinish = (values) => {
    setLoading(true);

    AUTH.post(`${process.env.GATSBY_API_USERS}${values.email}/recover`)
      .then((response) => {
        setSuccess(response.data?.message || 'Checkout your email to continue');
      })
      .catch((error) => {
        setFail(error.response.data.message);
      });
  };

  if (success || fail) {
    return (
      <>
        {success && (
          <Result
            status="success"
            title="Your request was submitted"
            subTitle={success}
          />
        )}
        {fail && (
          <Result
            status="error"
            title="Your request couldn't be submitted"
            subTitle={fail}
            extra={[
              <Button type="primary" key="reset">
                <Link to={routes.reset}>Try again</Link>
              </Button>,
            ]}
          />
        )}
      </>
    );
  }

  return (
    <Spin spinning={loading}>
      <Title title="Forgot Password" />

      <Form
        name="forgot"
        className={styles.form}
        size={isMobile ? 'large' : 'middle'}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            {
              required: true,
              message: 'Please input your email!',
            },
          ]}
        >
          <Input prefix={<MailOutlined />} placeholder="Email" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className={styles.button}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Spin>
  );
};
