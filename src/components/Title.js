import React from 'react';
import PropTypes from 'prop-types';

import styles from '../styles/components/title.module.css';

const Title = ({ title }) => <div className={styles.title}>{title}</div>;

export default Title;

Title.propTypes = {
  title: PropTypes.string.isRequired,
};
