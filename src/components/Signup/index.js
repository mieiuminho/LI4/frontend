import React, { Component } from 'react';
import { isMobile } from 'react-device-detect';
import { Form, Input, Button, Upload, Tooltip, message } from 'antd';
import { QuestionCircleOutlined, UploadOutlined } from '@ant-design/icons';
import Title from '../Title';

import { AUTH } from '../../utils/API';
import { isLoggedIn } from '../../utils/auth';
import { go } from '../../utils/browser';

import routes from '../../data/routes.json';
import styles from '../../styles/components/Signup/signup.module.css';

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 10,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const makeLabel = () => (
  <span>
    Username&nbsp;
    <Tooltip title="A unique id that identifies you in the app">
      <QuestionCircleOutlined />
    </Tooltip>
  </span>
);

const normFile = (e) => {
  const file = Array.isArray(e) ? e?.pop() : e?.fileList?.pop();

  if (file?.name) {
    file.name = file ? file?.name?.slice(-15) : undefined;
    if (file.status === 'error') {
      file.response = 'Server Error';
    }
    return [file];
  }
  return undefined;
};

export default class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if (isLoggedIn()) go(routes.tasks);

    const RegistrationForm = () => {
      const onFinish = (values) => {
        const key = 'notification';
        message.loading({ content: 'Login in...', key });

        AUTH.post(`${process.env.GATSBY_API_AUTH_SIGN_UP}`, {
          username: values.username,
          name: values.name,
          email: values.email,
          password: values.password,
          avatar: values.upload
            ? values.upload[0].thumbUrl.slice(22)
            : undefined,
        })
          .then(function login(response) {
            if (Object.prototype.hasOwnProperty.call(response.data, 'jwt')) {
              localStorage.setItem('jwt', response.data.jwt);
              localStorage.setItem('username', values.username);
              localStorage.setItem('isLoggedIn', 'true');
              go(routes.tasks);
              message.success({
                content: `Welcome, ${values.name}!`,
                key,
                duration: 2,
              });
            }
          })
          .catch(function (error) {
            message.error({
              content: `${error.response.data.message}`,
              key,
              duration: 2,
            });
          });
      };

      return (
        <Form
          labelCol={formItemLayout.labelCol}
          wrapperCol={formItemLayout.wrapperCol}
          name="register"
          size={isMobile ? 'large' : 'middle'}
          className={styles.form}
          onFinish={onFinish}
          initialValues={{
            prefix: '353',
          }}
          scrollToFirstError
        >
          <Form.Item
            name="email"
            label="E-mail"
            rules={[
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
              {
                required: true,
                message: 'Please input your E-mail!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(
                      'The two passwords that you entered do not match!',
                    ),
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="name"
            label="Name"
            rules={[
              {
                required: true,
                message: 'Please input your real name!',
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="username"
            label={makeLabel()}
            rules={[
              {
                required: true,
                message: 'Please input your nickname!',
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="upload"
            label="Profile Picture"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            extra="Upload your profile picture here"
          >
            <Upload beforeUpload={() => false} name="logo" listType="picture">
              <Button>
                <UploadOutlined /> Click to upload
              </Button>
            </Upload>
          </Form.Item>

          <Form.Item wrapperCol={tailFormItemLayout.wrapperCol}>
            <Button type="primary" className={styles.button} htmlType="submit">
              Register
            </Button>
          </Form.Item>
        </Form>
      );
    };

    return (
      <>
        <Title title="Sign Up" />
        <RegistrationForm />
      </>
    );
  }
}
