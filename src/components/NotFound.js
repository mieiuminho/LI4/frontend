import React from 'react';
import { Link } from 'gatsby';
import { Result, Button } from 'antd';

import routes from '../data/routes.json';

export default () => (
  <Result
    status="404"
    title="Oops! Error 404"
    subTitle="We can't seem to find the page you're looking for."
    extra={
      <Link to={routes.home}>
        <Button type="primary">Back Home</Button>
      </Link>
    }
  />
);
