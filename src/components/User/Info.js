import React from 'react';
import PropTypes from 'prop-types';
import { Row, Typography, Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';

import styles from '../../styles/components/User/info.module.css';

const { Title, Text } = Typography;

const Info = ({ user }) => (
  <>
    {user ? (
      <>
        <Row justify="center" align="middle">
          <Avatar size={200} src={user.avatar} icon={<UserOutlined />} />
        </Row>
        <Row justify="center" align="middle">
          <Title className={styles.name} level={2}>
            {user.name}
          </Title>
        </Row>
        <Row justify="center" align="middle">
          <Text strong type="secondary">
            @{user.username}
          </Text>
        </Row>
      </>
    ) : null}
  </>
);

Info.propTypes = {
  user: PropTypes.object.isRequired,
};

export default Info;
