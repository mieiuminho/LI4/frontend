import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Result } from 'antd';

import Info from './Info';

import API from '../../utils/API';

import styles from '../../styles/components/User/user.module.css';

export default class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      server: {
        error: false,
        status: '',
        message: '',
      },
      user: {},
    };
  }

  componentDidMount() {
    const { username } = this.props;

    API()
      .get(`${process.env.GATSBY_API_USERS}${username}`)
      .then((response) => {
        const user = response.data;
        console.log(response);
        console.log(user);
        this.setState({ user });
      })
      .catch((error) => {
        const { status, message } = error.response.data;
        this.setState({ server: { error: true, message, status } });
      });
  }

  render() {
    const { user, server } = this.state;

    return (
      <>
        {server.error ? (
          <Result
            status={
              (server.status === 403 && '403') ||
              (server.status === 404 && '404') ||
              '500'
            }
            title={server.status}
            subTitle={server.message}
          />
        ) : (
          <div className={styles.container}>
            <Info user={user} />
          </div>
        )}
      </>
    );
  }
}

User.propTypes = {
  username: PropTypes.string.isRequired,
};
