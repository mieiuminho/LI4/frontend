import React, { Component } from 'react';
import { isMobile } from 'react-device-detect';
import { Link } from 'gatsby';
import { Form, Input, Button, Checkbox, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import Title from '../Title';

import { AUTH } from '../../utils/API';
import { isLoggedIn } from '../../utils/auth';
import { go } from '../../utils/browser';

import routes from '../../data/routes.json';
import styles from '../../styles/components/Login/login.module.css';

export default class Workspaces extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if (isLoggedIn()) go(routes.tasks);

    const NormalLoginForm = () => {
      const onFinish = (values) => {
        const key = 'notification';
        message.loading({ content: 'Login in...', key });

        AUTH.post(`${process.env.GATSBY_API_AUTH_SIGN_IN}`, {
          username: values.username,
          password: values.password,
        })
          .then(function login(response) {
            if (Object.prototype.hasOwnProperty.call(response.data, 'jwt')) {
              if (values.remember) {
                localStorage.setItem('jwt', response.data.jwt);
                localStorage.setItem('username', values.username);
                localStorage.setItem('isLoggedIn', 'true');
              } else {
                sessionStorage.setItem('jwt', response.data.jwt);
                sessionStorage.setItem('username', values.username);
                sessionStorage.setItem('isLoggedIn', 'true');
              }
              go(routes.tasks);
              message.success({
                content: `Welcome back!`,
                key,
                duration: 2,
              });
            }
          })
          .catch(function (error) {
            message.error({
              content: `${error.response.data.message}`,
              key,
              duration: 2,
            });
          });
      };

      return (
        <Form
          name="normal_login"
          className={styles.form}
          size={isMobile ? 'large' : 'middle'}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your Username!',
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your Password!',
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Form.Item
              className={styles.remember}
              name="remember"
              valuePropName="checked"
              noStyle
            >
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Link to={routes.reset} className={styles.forgot}>
              Forgot password
            </Link>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" className={styles.button}>
              Log in
            </Button>
            Or <Link to={routes.signup}>register now!</Link>
          </Form.Item>
        </Form>
      );
    };

    return (
      <>
        <Title title="Log In" />
        <NormalLoginForm />
      </>
    );
  }
}
