import React from 'react';
import PropTypes from 'prop-types';

import { isLoggedIn } from '../utils/auth';
import { go } from '../utils/browser';

import routes from '../data/routes.json';

const PrivateRoute = ({ component: Component, location, ...rest }) => {
  if (!isLoggedIn() && location.pathname !== routes.login) {
    go(routes.login);
    return null;
  }

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <Component {...rest} />;
};

PrivateRoute.propTypes = {
  component: PropTypes.any.isRequired,
  location: PropTypes.object,
};

export default PrivateRoute;
