import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { isMobile } from 'react-device-detect';
import { Link } from 'gatsby';
import { Form, Input, Button, Result, Spin } from 'antd';
import { LockOutlined } from '@ant-design/icons';
import Title from '../Title';

import { AUTH } from '../../utils/API';

import routes from '../../data/routes.json';
import styles from '../../styles/components/Login/login.module.css';

const Reset = ({ token }) => {
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState('');
  const [fail, setFail] = useState('');

  const onFinish = (values) => {
    setLoading(true);

    AUTH.post(`${process.env.GATSBY_API_USERS}recover/${token}`, {
      password: values.password,
    })
      .then((response) => {
        setSuccess(response.data?.message || 'Password changed correctly');
      })
      .catch((error) => {
        setFail(error.response.data.message);
      });
  };

  if (success || fail) {
    return (
      <>
        {success && (
          <Result
            status="success"
            title="You are all set"
            subTitle={success}
            extra={[
              <Button type="primary" key="login">
                <Link to={routes.login}>Log in</Link>
              </Button>,
            ]}
          />
        )}
        {fail && (
          <Result
            status="error"
            title="We couldn't change your password"
            subTitle={fail}
            extra={[
              <Button type="primary" key="reset">
                <Link to={routes.reset}>Try again</Link>
              </Button>,
            ]}
          />
        )}
      </>
    );
  }

  return (
    <Spin spinning={loading}>
      <Title title="Reset Password" />

      <Form
        name="reset"
        className={styles.form}
        size={isMobile ? 'large' : 'middle'}
        onFinish={onFinish}
      >
        <Form.Item
          name="password"
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please input your new password!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item
          name="confirm"
          hasFeedback
          dependencies={['password']}
          rules={[
            {
              required: true,
              message: 'Please confirm your new password!',
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error('The two passwords that you entered do not match!'),
                );
              },
            }),
          ]}
        >
          <Input
            prefix={<LockOutlined />}
            type="password"
            placeholder="Confirm Password"
          />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className={styles.button}>
            Reset
          </Button>
        </Form.Item>
      </Form>
    </Spin>
  );
};

Reset.propTypes = {
  token: PropTypes.string,
};

export default Reset;
