import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';

import '../styles/global.css';
import Navbar from '../components/Navbar';

import styles from '../styles/layouts/layout.module.css';

const { Footer, Content } = Layout;

const BaseLayout = ({ children }) => (
  <Layout>
    <Navbar />
    <Content className={styles.content}>{children}</Content>
    <Footer className={styles.footer}>
      &copy; delegatewise {new Date().getFullYear()}
    </Footer>
  </Layout>
);

BaseLayout.propTypes = {
  children: PropTypes.any.isRequired,
};

export default BaseLayout;
